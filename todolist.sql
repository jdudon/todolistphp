-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: localhost    Database: todolist
-- ------------------------------------------------------
-- Server version	8.0.30-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `task` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `date_crea` int DEFAULT NULL,
  `date_end` int DEFAULT NULL,
  `is_checked` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (22,'Tache 1','Nulla in dictum sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis at finibus dolor, at tempus metus. Aliquam condimentum tristique ipsum at aliquam. Phasellus et hendrerit dolor, nec laoreet nibh. Aliquam in feugiat ligula. Fusce ac urna condimentum, dignissim nisi aliquam, feugiat augue. Vestibulum ac egestas turpis, at egestas ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. ',1665137112,NULL,_binary '\0'),(24,'Aller chez le vétérinaire','\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque turpis ligula, pellentesque eu metus eu, facilisis placerat enim. Nullam mattis luctus consequat. Aliquam vitae porta augue. Vivamus convallis purus varius, egestas enim ac, consectetur dui. Vivamus id ultrices lacus, ut consequat nulla. Nullam dapibus ac nibh at venenatis. Nunc nec dapibus velit. ',1665135924,NULL,_binary '\0'),(25,'Aller chez le vétérinaire','\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque turpis ligula, pellentesque eu metus eu, facilisis placerat enim. Nullam mattis luctus consequat. Aliquam vitae porta augue. Vivamus convallis purus varius, egestas enim ac, consectetur dui. Vivamus id ultrices lacus, ut consequat nulla. Nullam dapibus ac nibh at venenatis. Nunc nec dapibus velit. ',1665135980,NULL,_binary '\0'),(26,'Nouvelle nouvelle tacghe','Nulla in dictum sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis at finibus dolor, at tempus metus. Aliquam condimentum tristique ipsum at aliquam. Phasellus et hendrerit dolor, nec laoreet nibh. Aliquam in feugiat ligula. Fusce ac urna condimentum, dignissim nisi aliquam, feugiat augue. Vestibulum ac egestas turpis, at egestas ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. ',1665137090,NULL,_binary '\0');
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-26  9:45:54
